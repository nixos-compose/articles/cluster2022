# TO REPRODUCE

## With Nix

1. Install Nix: https://nixos.org/download.html

2. Enable Nix Flakes: https://nixos.wiki/wiki/Flakes

3. Put everything in this repo in git: `git init && git add *` (Nix Flakes need git to work)

3. Enter the development environment: `nix develop .#rshell`

4. In the environment: `Rscript script.R`

## Without Nix

You will need R (we used 4.1.2) with the `tidyverse` and `reshape2` libraries.

```
> packageVersion("tidyverse")
[1] ‘1.3.1’
> packageVersion("reshape2")
[1] ‘1.4.4’
```

```
> library(tidyverse)
✔ ggplot2 3.3.5     ✔ purrr   0.3.4
✔ tibble  3.1.6     ✔ dplyr   1.0.7
✔ tidyr   1.1.4     ✔ stringr 1.4.0
✔ readr   2.0.2     ✔ forcats 0.5.1
```
