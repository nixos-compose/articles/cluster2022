{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nixpkgs2111.url = "github:nixos/nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs, nixpkgs2111 }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    pkgs2111 = import nixpkgs2111 { inherit system; };
    rPkgs = with pkgs2111.rPackages; [
      rlang
      tidyverse
      reshape2
    ];
    myR = pkgs2111.rWrapper.override { packages = rPkgs; };
  in
  {
    devShells.${system} = {
      rshell = pkgs.mkShell {
        buildInputs = with pkgs; [
          myR
        ];
      };
    };
  };
}
