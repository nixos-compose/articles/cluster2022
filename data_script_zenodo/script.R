library(tidyverse)
library(reshape2)


plot_compa_kameleon <- read_csv(
    "./data_kameleon.csv",
    col_names = c("solution", "fs", "target", "build_time", "rebuild_time", "build_size", "rebuild_size")
    ) %>%
    mutate(
        type = paste(solution, target, sep="-"),
        build_size = build_size / 1024,
        rebuild_size = rebuild_size / 1024,
    ) %>%
    select(-solution, -target) %>%
    melt(id.vars = c("type", "fs")) %>%
    separate(variable, sep = "_", into = c("phase", "variable")) %>%
    mutate(
        phase = factor(phase, levels = c("rebuild", "build")),
    ) %>%
    unite(fs, variable, col=variable, sep = "_") %>%
    filter(variable != "without_nfs_size") %>%
    mutate(variable = factor(
                        variable,
                        levels = c(
                            "with_nfs_time",
                            "without_nfs_time",
                            "with_nfs_size"
                        ))
    ) %>%
    group_by(type, phase, variable) %>%
    summarise(
        mean_value = mean(value),
        bar = 2.576 * sd(value) / sqrt(n())
    ) %>%
    ggplot(aes(x = mean_value, y = factor(type), fill = phase)) +
    geom_col(position = "dodge") +
    geom_errorbar(
        aes(xmin = mean_value - bar , xmax = mean_value + bar),
        width = 0.5,
        position=position_dodge(.9)
    ) +
    facet_wrap(~ variable,
               scales = "free_x",
               labeller = as_labeller(
                c(
                    with_nfs_size = "Image size [Mib]",
                    with_nfs_time = "Construction Time with NFS [s]",
                    without_nfs_time = "Construction Time without NFS [s]"
                )
            ),
    ) +
    theme_bw() +
    theme(
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.title.x = element_blank(),
        plot.title.position = "plot"
    ) +
    ylab("") +
    scale_y_discrete(
      name = NULL,
      breaks = c("nxc-g5k-ramdisk", "nxc-g5k-image", "kameleon-g5k-image"),
      labels = c("nxc-g5k-ramdisk", "nxc-g5k-image", "kameleon")
    ) +
    scale_fill_grey(
      name = "",
      start = 0.5,
      end = 0.8,
      breaks = c("build", "rebuild"),
      labels = c("base", "base + hello")
    ) +
    ggtitle(
        "Image Size, Construction and Reconstruction Time for Different Environments with and without NFS"#,
        # subtitle = "with 99% confidence intervals (5 repetitions)"
    )
ggsave("./eval_nxc_kameleon.pdf", plot = plot_compa_kameleon, width = 8.5, height = 3.5)



plot_compa_enoslib <- read_csv(
    "./data_enoslib.csv",
    col_names = c("solution", "app", "target", "Init", "Build", "Deploy", "Provisioning", "Run")
    ) %>%
    mutate(
        type = paste(solution, target, sep="-"),
        "Sub + Deploy" = Init + Deploy,
    ) %>%
    select(-solution, -target, -Init, -Deploy) %>%
    mutate(
        type = factor(type, levels = c("enoslib-g5k-image", "nxc-g5k-ramdisk", "nxc-g5k-image")),

    ) %>%
    melt(id.vars = c("type", "app")) %>%
    mutate(
        variable = factor(variable, levels = c("Build", "Sub + Deploy", "Provisioning", "Run")),
    ) %>%
    group_by(type, app, variable) %>%
    summarise(
        mean_value = mean(value),
        bar = 2.576 * sd(value) / sqrt(n())
    ) %>%
    ggplot(aes(x = variable, y = mean_value, fill = type)) +
    geom_col(position = "dodge") +
    geom_errorbar(
        aes(ymin = mean_value - bar , ymax = mean_value + bar),
        width = 0.5,
        position=position_dodge(.9)
    ) +
    facet_wrap(~ app) +
    theme_bw() +
    theme(
        legend.position = "bottom",
        strip.background = element_blank(),
        plot.title.position = "plot",
        strip.text.x = element_text(size = 12)
    ) +
    ylab("Time [s]") +
    xlab("Phases") +
    scale_fill_grey(
      name = "",
      start = 0.5,
      end = 0.8,
      breaks = c("enoslib-g5k-image", "nxc-g5k-ramdisk", "nxc-g5k-image"),
      labels = c("EnOSlib", "nxc-g5k-ramdisk", "nxc-g5k-image")
    ) +
    ggtitle(
        "Time Spent in each Phases for Different Approaches with 99% Confidence Intervals (5 repetitions)"
        #subtitle = "with 99% confidence intervals (5 repetitions)"
    )
ggsave("./eval_nxc_enoslib.pdf", plot = plot_compa_enoslib, width = 8, height = 3)

df_store_content_vm_ramdisk <- read_csv("./content_store_vm-ramdisk.csv", col_names = c("size", "path"))
df_store_content_g5k_ramdisk <- read_csv("./content_store_g5k-ramdisk.csv", col_names = c("size", "path"))
df_store_content_g5k_image <- read_csv("./content_store_g5k-image.csv", col_names = c("size", "path"))

df_store_content_common <- intersect(df_store_content_g5k_image, intersect(df_store_content_g5k_ramdisk, df_store_content_vm_ramdisk))
df_store_content_common_g5k <- intersect(df_store_content_g5k_image, df_store_content_g5k_ramdisk)

heavy_packages <- rbind(
        df_store_content_vm_ramdisk,
        df_store_content_g5k_ramdisk,
        df_store_content_g5k_image
    ) %>%
    unique() %>%
    slice_max(size, n = 7)

plot_melissa <- rbind(
        df_store_content_vm_ramdisk %>% mutate(flavour = "vm-ramdisk"),
        df_store_content_g5k_ramdisk %>% mutate(flavour = "g5k-ramdisk"),
        df_store_content_g5k_image %>% mutate(flavour = "g5k-image")
    ) %>%
    mutate(
        state = if_else(
                    path %in% df_store_content_common$path,
                    "common",
                    if_else(
                        path %in% df_store_content_common_g5k$path,
                        "common-g5k",
                        flavour))
    ) %>%
    mutate(
        path = ifelse(path %in% heavy_packages$path, path, "others-others")
    ) %>%
    separate(path, into = c("base", "pname"), sep = "-") %>%
    group_by(state, pname, flavour) %>%
    summarise(size = sum(size)) %>%
    mutate(
        pname = if_else(pname == "others",
                        sprintf("others-%s (%.0f MiB)", state, (size / 1024)),
                        paste(pname, sprintf("(%.0f MiB)", (size / 1024)), sep = " ")),
        state_legend = if_else(state == flavour, "specific", state),
        state_legend = factor(state_legend, levels = c("specific", "common-g5k", "common")),
        flavour = factor(flavour, levels = c("vm-ramdisk", "g5k-ramdisk", "g5k-image")),
        size = size / (1024 * 1024),
    ) %>%
    ggplot(aes(x = flavour, y = size, fill = state_legend)) +
    geom_bar(stat = "identity", position = "stack", color = "black") +
    geom_text(position = position_stack(vjust = .5), color = "black", size = 3.35, aes(label = pname)) +
    theme_bw() +
    theme(
        legend.position = "bottom",
        plot.title.position = "plot",
        legend.text=element_text(size=15),
        plot.title=element_text(size=19),
        axis.text=element_text(size=15),
        axis.title=element_text(size=15)
    ) +
    scale_fill_manual(
        name = "",
        breaks = c("common", "common-g5k", "specific"),
        labels = c("Common", "Common G5K", "Flavour Specific"),
        values = c("specific" = "#d0d0d0", "common-g5k" = "#a0a0a0", "common" = "#f0f0f0")
    ) +
    xlab("Flavours") +
    ylab("Size [GiB]") +
    ggtitle("Content of the Nix Store of the Melissa Image for each Flavour")
ggsave("./plot_melissa.pdf", plot = plot_melissa, width = 7.5, height = 8)

# compute speedup factors for nxc vs kam
data_comp_kameleon <- read_csv("./data_kameleon.csv",
  col_names = c("solution", "fs", "target", "build_time", "rebuild_time", "build_size", "rebuild_size")
) %>% mutate(
  type = paste(solution, target, sep="-"),
  build_size = build_size / 1024,
  rebuild_size = rebuild_size / 1024,
)

# rebuild time / build time speedup
data_comp_kameleon %>% group_by(solution,fs,target) %>% summarize(
  mean_build_time = mean(build_time),
  mean_rebuild_time = mean(rebuild_time),
) %>% mutate(
  rebuild_time_speedup = mean_build_time / mean_rebuild_time
)

# nxc vs kam speedup factor
d = data_comp_kameleon %>% filter(target=="g5k-image" & fs=="without_nfs") %>% group_by(solution) %>% summarize(
  mean_build_time = mean(build_time),
  mean_rebuild_time = mean(rebuild_time),
)
(d %>% filter(solution == "kameleon"))$mean_build_time / (d %>% filter(solution == "nxc"))$mean_build_time
(d %>% filter(solution == "kameleon"))$mean_rebuild_time / (d %>% filter(solution == "nxc"))$mean_rebuild_time
