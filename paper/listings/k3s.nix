{ pkgs, ... }:
let k3sToken = "df54383b5659b9280aa1e73e60ef78fc";
in {
  nodes = {
    # Configuration of the server
    server = { pkgs, ... }: {
      environment.systemPackages = with pkgs; [
        k3s gzip # List of the packages to include
      ];
      networking.firewall.allowedTCPPorts = [
        6443 # Ports to open
      ];
      services.k3s = {
        # Definition of the k3s service
        enable = true;
        role = "server";
        package = pkgs.k3s;
        extraFlags = "--agent-token ${k3sToken}";
      };
    };
    # Configuration of the agent
    agent = { pkgs, ... }: {
      environment.systemPackages = with pkgs; [
        k3s gzip
      ];
      services.k3s = {
        enable = true;
        role = "agent";
        serverAddr = "https://server:6443";
        token = k3sToken;
      };
    };
  };
}
