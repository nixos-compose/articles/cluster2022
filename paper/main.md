---
title: "Painless Transposition of Reproducible Distributed Environments with NixOS Compose"
author:
  - name: "Quentin Guilloteau, Jonathan Bleuzen, Millian Poquet, Olivier Richard"
    affiliation: Univ. Grenoble Alpes, Inria, CNRS, LIG
    location: 38000 Grenoble France
    email: Firstname.Lastname@inria.fr
  # - name: "Anonymous Author(s)"
  #   affiliation: Anonymous Affiliation(s)
  #   location: Anonymous Location(s)
  #   email: Anonymous Email Address(es)
keywords:
  - Reproducibility
  - Distributed Systems
  - System Image
  - Deployment
  - Nix
  
numbersections: yes
fontsize: 10pt
papersize: a4paper
classoption: conference
lang: en
bibliography: references.bib
abstract: |
    Development of environments for distributed systems is a tedious and time-consuming iterative process.
    The \repro\ of such environments is a crucial factor for rigorous scientific contributions.
    We think that being able to smoothly test environments both locally and on a target distributed platform makes development cycles faster and reduces the friction to adopt better experimental practices.
    To address this issue, this paper introduces the notion of environment transposition and implements it in \nxc, a tool that generates reproducible distributed environments.
    It enables users to deploy their environments on virtualized (docker, QEMU) or physical (\grid) platforms with the same unique description of the environment.
    We show that \nxc\ enables to build reproducible environments without overhead by comparing it to state-of-the-art solutions for the generation of distributed environments (\enos\ and \kam).
    \nxc\ actually enables substantial performance improvements on image building time over \kam\ (up to 11x faster for initial builds and up to 19x faster when building a variation of an existing environment).
header-includes: |
  \usepackage{booktabs}
  \usepackage{longtable}
  \usepackage{multirow}
  \usepackage{hyperref}
  \usepackage{xcolor}
  \usepackage{listings}
  \usepackage{algorithm2e}
  \usepackage[inline]{enumitem}
  \newcommand{\io}{\emph{I/O}}
  \newcommand{\qos}{\emph{Quality-of-Service}}
  \newcommand{\repro}{reproducibility}
  \newcommand{\Repro}{Reproducibility}
  \newcommand{\transpo}{\emph{Transposition}}
  \newcommand{\flavour}{\emph{flavour}}
  \newcommand{\flavours}{\emph{flavours}}
  \newcommand{\ie}{\emph{i.e.,}}
  \newcommand{\eg}{\emph{e.g.,}}
  \newcommand{\nix}{\emph{Nix}}
  \newcommand{\oar}{\emph{OAR}}
  \newcommand{\nixos}{\emph{NixOS}}
  \newcommand{\nxc}{\emph{NixOS Compose}}
  \newcommand{\enos}{\emph{EnOSlib}}
  \newcommand{\grid}{\emph{Grid'5000}}
  \newcommand{\kam}{\emph{Kameleon}}
  \newcommand{\kad}{\emph{Kadeploy}}
  \newcommand{\mel}{\emph{Melissa}}
  \newcommand{\store}{\emph{Nix Store}}
  \newcommand\todo[1]{{\textcolor{red}{TODO: #1}}}
  \definecolor{commentcolour}{rgb}{0.04,0.43,0.17}
  \definecolor{keywordcolour}{rgb}{0.65,0.15,0.64}
  \definecolor{backcolour}{rgb}{1,1,1}
  \definecolor{linenumbercolour}{rgb}{0.1,0.1,0.1}
  \definecolor{stringcolour}{rgb}{0.56,0.06,0.49}
  \lstdefinestyle{mystyle}{
      backgroundcolor=\color{backcolour},   
      commentstyle=\color{commentcolour},
      keywordstyle=\color{keywordcolour},
      numberstyle=\tiny\color{linenumbercolour},
      stringstyle=\color{stringcolour},
      basicstyle=\ttfamily\footnotesize,
      breakatwhitespace=false,         
      breaklines=true,                 
      captionpos=b,                    
      keepspaces=true,                 
      numbers=left,                    
      numbersep=5pt,                  
      showspaces=false,                
      showstringspaces=false,
      showtabs=false,                  
      tabsize=2,
      framexrightmargin=-12pt
  }
  \lstset{style=mystyle, frame=single}
---




