# Presentation of \nxc\ {#sec:pres_nxc}

This section gives the main concepts and terminology of \nxc.
A \textbf{software environment} is a set of applications, libraries, configurations and services.
We define the \textbf{notion of \transpo} as the capacity to deploy a \emph{uniquely defined environment} on several platforms of different natures.
For example one may want to deploy an environment on local virtual machines to test and develop, and then want to deploy onto real machines on a distributed platform with the \emph{exact same description}.


## Concepts of \nix\ and \nixos

This section presents concepts from \nix\ and \nixos\ that are required for a better understanding of the \nxc\ tool.

\begin{figure}
    \centering
    \includegraphics[width = 0.48\textwidth]{./figs/package-datamove.pdf}
    \caption{Comparison between traditional package managers and \nix. Traditional package managers fetch a built version of the package from a mirror, but information on how they have been built is unknown. In the case of \nix, the package is described as a \nix\ function that takes as input the source code and how to build it. If the package with these inputs has already been built and is available in the \nix\ caches (equivalents of mirrors) it is simply downloaded to the \store. Otherwise, it is built locally and added to the \store.}\label{fig:nix_packages}
\end{figure}

A \nix\ package is defined by a \textbf{function} taking as input the sources and dependencies, as well as how to construct it, and returning the package.
\nix\ fetches or rebuilds the dependencies and then executes \emph{in isolation} the commands to build the package.
The packages are then stored and isolated in a special directory called the \store.
The \store\ is read-only, which by design makes impossible the alteration of packages after the build.
Figure \ref{fig:nix_packages} summarizes the differences between traditional package manager and \nix.
A hash code of a package inputs (sources, build script, \nix\ expression) are associated with each package.
This enables the cohabitation of multiple versions of the same package on the same machine.

A \textbf{\nix\ system profile} defines the configuration of the system (packages, \texttt{initrd}, etc.).
Among many features, a profile can define filesystems such as NFS and mount them automatically at boot time.
Figure \ref{fig:nix_store} depicts an example of user profile containing the Batsim application \cite{dutot:hal-01333471}, which requires the SimGrid \cite{casanova:hal-01017319} library at runtime.
A \nixos\ image can contain several profiles and \nix\ can switch between them by modifying symbolic links and restarting services via \texttt{systemd}.

\begin{figure}
    \centering
    \includegraphics[width = 0.42\textwidth]{./figs/store.pdf}
    \caption{Figuration of the \store\ content when the \texttt{alice} user has installed a Batsim \cite{dutot:hal-01333471} binary in her profile. As Batsim requires the SimGrid \cite{casanova:hal-01017319} library at runtime, SimGrid must also be in the store. Packages are stored in their own subdirectory, but common dependencies are not duplicated as symbolic links and shared libraries are used.
}\label{fig:nix_store}
\end{figure}


\begin{figure*}
    \centering
    \includegraphics[width = 0.75\textwidth]{./figs/workflow_bw.pdf}
    \caption{Workflow of \nxc. Local development of the environment is done using light and fast development with containers and virtual machines. Once the description of the environment (composition) has been tested, deployments on a distributed platform can be done with the \textbf{exact same interface}.}\label{fig:nxc_workflow}
\end{figure*}

## Concepts of \nxc

\nxc\ is based on \nix\ and \nixos\ to apply the notion of \transpo\ to distributed systems.
As depicted on Figure \ref{fig:nxc_motiv}, it enables users to have a **single definition** of their environment and to **deploy it to different platforms**.
For the sake of clarity, \nxc concepts will be illustrated on an example environment that contains \emph{k3s} \cite{k3s}, a lightweight version of Kubernetes for the orchestration of containers.

\begin{minipage}{\linewidth}
\lstinputlisting[language=Python, caption = \nxc\ composition file example for \emph{k3s} \cite{k3s}. , label = {lst:k3s}]{./listings/k3s.nix}
\end{minipage}

### Role

A \textbf{role} is a type of configuration associated with the mission of a node.
\emph{k3s} is a client-server application where clients are named \emph{agents}.
There would therefore be 2 roles: one for the \texttt{server} and another for the \texttt{agent}s.
As all the agents have the same configuration they can use the same role.
Note that in cases where there is one node per role, the notion of role and the notion of node overlap.

### Composition {#sec:composition}

A \textbf{composition} is a \nix\ expression describing the \nixos\ configuration of every role in the environment.
Listing \ref{lst:k3s} shows an example of composition for \emph{k3s}.
Please note that the composition in Listing \ref{lst:k3s} contains the \texttt{nodes} keyword on line 4 to define the various roles (instead of the \texttt{roles} keyword).
This is done on purpose to make \nxc's syntax retro-compatible with the syntax used in \emph{NixOS tests}\footnote{\url{https://nixos.org/guides/integration-testing-using-virtual-machines.html}}
(these tests are part of \nixos and enable to start virtual machines with QEMU with the defined environment, and execute a Python script to test the application inside the environment).
The composition in Listing \ref{lst:k3s} defines the open port of the \texttt{server} (line 10-12), the available packages (lines 7-9 and 23-25, packages are \texttt{k3s} and \texttt{gzip}), as well as the \texttt{systemd} services (lines 13-19 and 26-31).
The \texttt{k3s} service is not defined explicitly in this example, as we reuse an existing definition that is available in the collection of \nix\ expressions called \emph{nixpkgs}\footnote{\url{https://github.com/NixOS/nixpkgs}}.
For personal applications users might have to define their own \texttt{systemd} services,
which can be done declaratively via a \nix\ expression.
\nix variables can be used to avoid information duplication,
as seen for the \emph{k3s} token (used to manage authentication) defined on line 2 and used to configure both the \texttt{server} (line 18) and the \texttt{agents} (line 30).

### Deployment

A \textbf{deployment} assigns a role to every node.
\nxc proposes two ways to define deployment as YAML files, as illustrated on Listings \ref{lst:yaml} and \ref{lst:yaml_names}.
The first way is to define the number of nodes that should be used for each role (Listing \ref{lst:yaml}).
This is convenient when working on homogeneous nodes for simple deployments.
For the sake of \repro, \nxc\ generates a deterministic assignment -- if the same nodes are reserved and the same deployment is used, the role of each node remains the same.
The second way to define a deployment is to directly define the role that each node should take (Listing \ref{lst:yaml_names}).
This is more suited for complex scenarios, as it enables users to generate their deployment file depending on their needs.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Deployment file example for the \emph{k3s} example with 1 server and 3 agents where the users defined the quantity of nodes per role. The hostnames are generated by \nxc. In this example there would be 3 agents with the hostnames \texttt{agent1}, \texttt{agent2} and \texttt{agent3}.}, label = {lst:yaml}]
# Users can define the number of nodes per role
server: 1
agent: 3
\end{lstlisting}
\end{minipage}

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Deployment file example for the \emph{k3s} example with 1 server and \\ 3 agents where the users defined the hostnames of every node per role.}, label = {lst:yaml_names}]
# Users can also define the hostnames per role
server: 1
agent:
    - agent1
    - agent2
    - agent3
\end{lstlisting}
\end{minipage}

### Flavours

A \textbf{flavour} is a target for the deployment of the environment.
This notion includes the (virtual or physical) platform onto which the deployment should be done, and also the \emph{deployment method} that should be used (\eg\ full system image or ramdisk).
As we write these lines \nxc\ supports the following flavours:

- \texttt{docker} for \texttt{docker-compose} \cite{docker-compose} configurations.

- \texttt{vm-ramdisk} for in-memory QEMU virtual machines.

- \texttt{g5k-image} for full system tarball images that can be deployed on \grid \cite{grid5000} via \kad \cite{georgiou2006tool}.

- \texttt{g5k-ramdisk} for \texttt{initrd}s that can be quickly deployed in memory without the need to reboot the host machine on \grid (via the \texttt{kexec} syscall).

During the development phase of the environment, users can deploy \emph{locally, lightly and quickly} with the \texttt{docker} and \texttt{vm-ramdisk} flavours.
At a later stage, users can test their environment on real nodes from the \grid testbed with the \texttt{g5k-ramdisk}, which is convenient for trial-and-error operations thanks to its fast boot time.
Finally, the environment can be deployed at real scale on \grid with the \texttt{g5k-image} flavour.
Please note that some flavours have \repro limitations due to the underlying technologies.
For example, controlling the version of the Linux kernel is impossible when using the \texttt{docker} flavour.

## Workflow of \nxc

This section presents the workflow of \nxc\ and how it enables users to simply transpose their environment from one platform to another.

### Local Testing {#sec:local}

When developing an environment, users can work with the \texttt{docker} and \texttt{vm-ramdisk} flavours with the following workflow:

1. Building the image: \texttt{nxc build -f docker} or \texttt{nxc build -f vm-ramdisk}

2. Deploy the environment: \texttt{nxc start}.
By default, \nxc\ takes the last composition built.

3. Connect to the nodes: \texttt{nxc connect [node name]}.
This opens a connection to the desired node.
If name is omitted, a terminal multiplexer\footnote{\texttt{tmux}: \url{https://github.com/tmux/tmux}} opens with one pane per node, which is convenient to run commands on the nodes.

### Distributed Deployment {#sec:distributed}


Once the environment has been tested with local flavours, it can be tested in a distributed system.

1. Building the image: \texttt{nxc build -f g5k-ramdisk} or \texttt{nxc build -f g5k-image}

2. Reservation of the nodes to use for the deployment: depends on your platform resource manager. For example \texttt{salloc} for *Slurm* \cite{yoo2003slurm} or \texttt{oarsub} for \oar\ \cite{capit_batch_2005}.

3. Deploy the environment: \texttt{nxc start}.

4. Connect to the nodes: \texttt{nxc connect [node name]}.

Figure \ref{fig:nxc_workflow} summarizes the \nxc\ workﬂow.
\nxc\ aims at making the transition between platforms as seamless as possible.
Thus, the workflow in a distributed setting (Section \ref{sec:distributed}) \textbf{is identical} to the workflow in a local one (Section \ref{sec:local}).
The only difference is that in a distributed setting, users need to first reserve the resources before deploying.
