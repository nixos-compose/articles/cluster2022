# Introduction

The scientific community as a whole has been traversing a \repro\ crisis for the last decade.
Computer science does not make an exception.
While scientists can accomplish reproducibility in different manners, it requires additional work and discipline.
In 2015, Collberg et al. \cite{collberg_repeatability_2015} studied the \repro\ of 402 articles published in journals and conferences on computer systems.
Each of those articles linked the source code used to produce the results. Out of these 402 papers, Collberg et al. could not reproduce 46 \%. The causes were: \emph{(i)} the source code was unavailable, \emph{(ii)} the source code did not compile or run, and \emph{(iii)} the experiments required specific hardware.

In this article, we tackle problem \emph{(ii)} in the context of distributed systems.
We aim to make the **entire** software stack involved in a experiment of distributed systems reproducible.
This implies making the compilation and the deployment of this stack reproducible, allowing one to rerun the experiment on an identical environment in one week or ten years.

To reach this goal, we exploit the declarative approach for system configuration provided by the \nixos\ \cite{nixos_2008} Linux distribution.
\nixos\ makes use of a configuration that describes the environment of the entire system, from user-space to the kernel.
The distribution is itself based on the purely functional \nix\ package manager \cite{dolstra_nix_2004}.
The definition of packages (or system configuration in the case of \nixos) are functions without side effects,
which enables \nix\ and \nixos\ to reproduce the exact same software when the same inputs are given.

We extend this notion of system configuration for distributed systems in a new tool named \nxc.
\nxc\ enables to define distributed environments and to deploy them on various platforms that can either be physical (\eg on the \grid\ testbed \cite{grid5000}) or virtualized (Docker or QEMU \cite{qemu}).
\nxc\ exposes the **exact same user interface** to define and deploy environments regardless of the targeted platform.
We think that this functionality paired with fast rebuild time of environments improves user experience,
and we hope that it will help the adoption of experimental practices that foster \repro.

The paper is structured as follows.
Section \ref{sec:sota} gives the state-of-the-art on tools related to the reproducible deployment of distributed systems, and positions \nxc in it.
Section \ref{sec:pres_nxc} presents \nxc, its main concepts, the external concepts it relies on, and the users' utilization workflow for setting up a complete reproducible system and software stack.
Section \ref{sec:how_it_works} gives technical details on how \nxc\ works.
Section \ref{sec:melissa} presents how \nxc\ can be used on a complex example that combines several distributed middlewares.
Section \ref{sec:eval} offers experimental performance results of \nxc\ against standard state-of-the-art tools using multiple metrics.
Finally, Section \ref{sec:conclu} concludes the paper with final remarks and perspectives on reproducible works and environments.
