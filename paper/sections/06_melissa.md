# A Complex Example: Melissa {#sec:melissa}

This section shows how complex distributed environments can be developed with \nxc
by taking the \mel\ \cite{terraz_melissa} framework as an example.

## Presentation of \mel

\mel\ is a framework to run large-scale sensitivity analyses.
Its main specificity is the online processing of data to limit usage of intermediate file storage, contrary to postmortem approaches.
It can be used in several HPC environments as it is compatible with two resource managers (RM): Slurm \cite{yoo2003slurm} and OAR \cite{capit_batch_2005}.
\mel\ implements a client/server model where the clients are simulations that generate and send data to a server that runs the statistics algorithms.

\nxc\ enables to deploy a resource manager (including all the components it requires, \eg\ a database), \mel, and all the components required by \mel\ at runtime (\eg\ a distributed file system).

```{=latex}
\begin{minipage}{\linewidth}
```
In the following example we deploy \mel\ with the Slurm resource manager.
Four roles are needed to define the environment.

- `server`: RM server and file system server

- `dbd`: MariaDB database (accounting for the RM)

- `computeNode`: worker node

- `frontend`: node from where initial jobs are submitted

```{=latex}
\end{minipage}
```

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Configuration of the \texttt{server} and \texttt{computeNode} roles.}, label = {lst:melissa-roles}]
server = { pkgs, ... }: {
  imports = [ slurmconfig nfsConfigs.server ];
  services.slurm.server.enable = true;
  systemd.services.slurmctld.serviceConfig = {
    Restart = "on-failure";
    RestartSec = 3;
  };
};
computeNode = { pkgs, ... }: {
  imports = [ slurmconfig nfsConfigs.client ];
  environment.systemPackages = [
    melissa melissa-heat-pde
  ];
  services.slurm.client.enable = true;
  systemd.services.slurmd.serviceConfig = {
    Restart = "on-failure";
    RestartSec = 3;
  };
};
\end{lstlisting}
\end{minipage}

Some roles share parts of their configuration, like `server` and `computeNode`.
They both use Slurm but their configuration differs in terms of services, as they respectively enable the `slurm.server` and `slurm.client` services.

\mel\ itself also needs to be part of the environment.
Unlike the *k3s* example shown on Listing \ref{lst:k3s}, \mel\ is not available in *nixpkgs* and thus needs to be packaged.
Listing \ref{lst:melissa-drv} is a snippet of \mel's package definition that notably defines which source code should be used (lines 5-9) and which build dependencies should be used (lines 10-13). The build commands (line 14) are omitted for the sake of readability.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Snippet of the package definition for \mel}, label = {lst:melissa-drv}]
{ pkgs, ... }:
pkgs.stdenv.mkDerivation rec {
  pname = "melissa-${version}";
  version = "0.7.1";
  src = pkgs.fetchgit {
    url="https://gitlab.inria.fr/melissa/melissa";
    rev="e6d09...";
    sha256="sha256-IiJad...";
  };
  buildInputs = with pkgs; [
    cmake gfortran python3 openmpi
    zeromq pkg-config libsodium
  ];
  # Build phases are omitted
}
\end{lstlisting}
\end{minipage}

Similarly, the `melissa-heat-pde` simulation application must also be in the environment (line 12 of Listing \ref{lst:melissa-roles}).
Finally, as a distributed filesystem is necessary in this environment, our composition imports a NFS module for the roles that need it.


## Key difficulties

This section emphasizes the advantages of using \nxc\ to deploy the \mel\ distributed environment.

### NFS Server

Setting up a NFS server with tools like \kam\ or \enos\ is cumbersome.
The users would first need to install the \texttt{nfs} tools on the nodes and define the NFS server.
Then, the users would have to mount the newly defined server on every client node.
These steps can be automated with scripts but that would be fragile.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Definition of the NFS server. It exposes the \texttt{/srv/shared} folder.}, label = {lst:nfs_server}]
# Configuration of the NFS server
nfsServer = {
  services.nfs.server.enable = true;
  services.nfs.server.exports =
    "/srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)";
  services.nfs.server.createMountPoints = true;
};
\end{lstlisting}
\end{minipage}

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={Mounting of the NFS server for the compute nodes. The local mounting point is \texttt{/data}.}, label = {lst:nfs_client}]
# Mouting of the NFS server
nfsClient = {
  fileSystems = {
    "/data" = {
      device = "server:/";
      fsType = "nfs";
    };
  };
};
\end{lstlisting}
\end{minipage}

The declarative definition of NFS with \nixos is based on `systemd` services (see Listings \ref{lst:nfs_server} and \ref{lst:nfs_client}).
This makes them easier to define and more robust as they can be restarted until they perform the mount succesfully in the case of the NFS server starting after the clients.

### Resource Manager

\mel\ runs on production clusters managed by resource managers such as Slurm and OAR.
However, experimenting on \mel's behavior in different scenarios requires controlling the resource manager part of the environment.
The installation of such systems is far from trivial as they include several distributed services that must interact, and each one of these services require configuration.
A composition can be made modular so that users can descriptively change the resource manager.
The same benefit is achievable for comparison studies of versions of the \mel\ framework,
as the \nix\ function that defines \mel\ can be written in such a way that it takes \mel's source code as a function input.

We have deployed the \mel composition we have written with \nxc on 13 nodes with the experimental setup described in Section \ref{sec:expe_setup}. The deployment took approximately 2 minutes with the `g5k-ramdisk` flavour.

## \mel Images Content Comparison

\nxc\ aims to provide the same environment on different target platforms.
This section analyses the content of the \store\ in the \mel\ images generated for every flavour, as the \store\ content represents the software environment available on the node.
The `docker` flavour is omitted here as the containers do not have a well-defined specific \store\ but mount the \store\ of the host machine instead.

\begin{figure}
    \centering
    \includegraphics[width = 0.5\textwidth]{./figs/plot_melissa.pdf}
    \caption{Packages present in the \store\ of the \mel\ image for the different flavours. The colors represent the packages common to the flavours. The smaller packages are gathered under the \texttt{others-*} name. The \texttt{docker} flavour is omitted as it mounts the \store\ of the host machine.}\label{fig:plot_melissa}
\end{figure}

Figure \ref{fig:plot_melissa} presents the content of the \store\ of the \mel\ image for the different flavours.
The smaller packages are gathered under the `others-*` name.
We can see that the vast majority of the software stack is shared by several flavours.
There is a common 2 GiB set of packages common to every flavour containing the \nixos\ definition and the dependencies of \mel.
The two flavours targeting the \grid\ platform need more packages, for example the firmware to use the nodes' hardware.
Then for each of the flavour, there is about 5 \% of the total \store\ size for packages specific to the flavour.
For example, deploying a `g5k-image` image requires a complete reboot of the host and to go through the boot loader, hence the presence of `grub` in this image.
