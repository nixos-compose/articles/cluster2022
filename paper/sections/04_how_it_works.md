# Technical Details of \nxc\ {#sec:how_it_works}

\nix\ can generate a \nixos\ configuration from a \nix\ expression, including the \texttt{boot} and \texttt{init} phases required to start the kernel.
\nix\ stores those phases in the \store\, which enables \nxc\ to call them later on.
\nxc\ works in two steps: *Building* and *Deployment*.
The building phase is done using \nix\ tools wrapped with Python for the command-line interface.
The deployment is fully done with Python and mostly consists in the interaction with the different deployment tools (\kad\, *docker-compose*, *QEMU*).
The \nix\ part is around 2000 lines of code, and the Python part around 4000.

The following section (\ref{sec:g5k-ramdisk}) details how \nxc\ manages \texttt{g5k-ramdisk}, the flavour that enables quick in-memory deployment without the need to reboot host machines on \grid.
Details are omitted for the other flavours, but please refer to Table \ref{tab:flavours} for a summary of the difference in the building and deployment phases for all supported flavours.

\begin{table*}
\centering
\caption{Table summarizing the different flavours with their building and deployment phases.}\label{tab:flavours}
\begin{tabular}[t]{l p{3.5cm}  p{5.5cm} p{4cm}}
 \toprule
 \multirow{2}*{Flavour} & \multicolumn{2}{c}{Phase} & \multirow{2}*{Comments}\\
 \cmidrule(lr){2-3}
  & Building & Deployment & \\
 \midrule
 \texttt{docker} & Generate~a~\texttt{docker-compose} configuration and \texttt{docker} containers. & Call the \texttt{docker-compose} application with the right arguments. & Fastest and light but limited in application due to virtualization.\\
 \midrule
 \texttt{vm-ramdisk} & Generate the kernel and \texttt{initrd} for the roles of the composition. & Create a virtual network with Virtual Distributed Ethernet (VDE) and starts the Virtual Machines with QEMU. & Fast but takes a lot of memory. Limited to a couple of VMs on a laptop.\\
 \midrule
 \texttt{g5k-ramdisk} & Generate the kernel and \texttt{initrd} for the roles of the composition. & Use \texttt{kexec} to quicky start the new kernel without rebooting. Send the deployment information through the kernel parameters. & Long to build but fast to deploy. \texttt{kexec} has \repro\ limitations and consumes a lot of memory which can be limiting for large images.\\
 \midrule
 \texttt{g5k-image} & Generate a tarball of the image of the composition. & Use \kad\ to deploy the image to the nodes. Send the deployment information through the kernel parameters. & Longer to build and deploy, but it has the best \repro\ properties.\\
 \bottomrule
\end{tabular}
\end{table*}

## Details on the \texttt{g5k-ramdisk} Flavour {#sec:g5k-ramdisk}

### Construction

\nxc\ uses \nix\ to evaluate the configuration of every role in the composition.
\nix\ then generates the kernel and the \texttt{initrd} of the profiles.

### Deployment

\nxc\ relies on the \texttt{kexec} Linux system call for this flavour.
\texttt{kexec} enables to boot a new kernel from the currently running one.
This skips the initialization of the hardware usually done by the BIOS, which avoids an entire reboot of the machines and greatly reduces the time to boot the new kernel.
The \texttt{kexec} commands takes as input the desired kernel, the kernel parameters and the \texttt{initrd}.
\nxc\ passes the kernel and \texttt{initrd} generated in the construction phase to \texttt{kexec}.

As \nxc\ produces a single image containing the profiles of all the roles, \nxc needs at deployment time to tell each node the role it should take.
To achieve this, we pass this information using the kernel parameters to set up environment variables based on the role.
\nxc\ also uses the kernel parameters to pass ssh keys and information about the other hosts (\eg\ the `/etc/hosts`).
There is however a size limit of 4096 bytes on the kernel parameters, which prevents us to use this method to send the deployment information to nodes when users want to deploy a lot of nodes.
To deal with this, \nxc\ starts a light HTTP server on the cluster frontend for the duration of the deployment.
We pass the URL of this server using the kernel parameters.
Then, the nodes query this server with \texttt{wget} to retrieve the information associated with their roles.
Note that the deployed images do not include a HTTP server but only the \texttt{wget} application to fetch the data.
Figure \ref{fig:http_server} represents how the nodes get the deployment information based on the quantity of nodes involved.

\begin{figure}
    \centering
    \includegraphics[width = 0.48\textwidth]{./figs/http_serverV2.pdf}
    \caption{
Mechanism for the nodes to get the deployment information.
For a small number of nodes, the information is passed via the kernel parameters.
For a higher number of nodes, this is not possible due to the size limit on the kernel parameters (4096 bytes).
In this case \nxc\ starts a light HTTP server on the frontend and passes its URL to the nodes via the kernel parameters.
The nodes then query this server to retrieve the deployment information.}\label{fig:http_server}
\end{figure}
