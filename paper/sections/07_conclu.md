# Conclusion and Future Work {#sec:conclu}

This article has presented \nxc, a tool that enables the generation of reproducible distributed environments.
We have showed that \nxc\ deploys the exact same software stack on various platforms of different natures, without requiring specific work from users.
The software stack is reconstructible by design, as \nxc\ inherits its \repro\ properties from \nix\ and \nixos.
Our experiments showed that \nxc's \repro and platform versatility properties are achieved without deployment performance overhead in comparison to the existing solutions \kam\ and \enos.

\nxc is a free software released under the MIT license.
As we write these lines, we think that its maturity level enables to deploy the distributed environment of many experiments.
However, \nxc currently only provides first-class support for \grid.
We would like to support baremetal and virtualized deployments on other experimental testbeds such as CloudLab \cite{cloudlab} and Chameleon \cite{chameleon}.

\nxc\ enables to build and deploy reproducible distributed environments.
This is crucial for conducting reproducible distributed experiments, but this is only a part of the bigger picture.
We plan to explore how \nxc\ can be coupled to other tools that solve other parts of this problem.
\enos\ is for example well-suited to control the dynamic part of complex distributed experiments but lacks \repro properties, which makes us think that a well-designed coupling may be beneficial for practitioners.

The experiments conducted in this article showed that build caches greatly improves \nxc's build times, and that properly using the filesystem is important for its performance.
From a cluster administration perspective, providing a shared \store\ between users would be very interesting to avoid data duplication and to prevent different \nxc\ users to build the same packages over and over.
There are many ways to implement a distributed shared \store\ and we think that exploring their trade-offs would provide valuable insights, as \repro improvements should not be done at the cost of a higher resource waste on clusters.

User experience is a crucial factor that must be considered for reproducible experimental practices to become the standard.
With this in mind, we think that the notion of \transpo\ we have defined in this article and implemented in \nxc\ is very beneficial.
\transpo reduces the development time of distributed environments, as it enables users to do most of the trial-and-error parts of this iterative process with fast cycles, without any \repro penalty on real-scale deployments.
However, practitioners that adopt \nxc\ are likely to experience a paradigm shift if they are not already accustomed to \nix's approach.
We strongly believe that the \repro and serenity gains it brings are worth it.
\newpage
