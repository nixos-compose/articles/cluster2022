FLAVOURS=("vm-ramdisk" "g5k-ramdisk" "g5k-image")

for flavour in "${FLAVOURS[@]}"
do
    nix build ./nxc#composition::${flavour} && cat result | jq .compositions_info.composition.all_store_info -r | xargs -I {} cat {}/merged-store-paths | xargs -I [] du -s [] | awk '{print $1 ", " $2;}' > content_store_${flavour}.csv
done
