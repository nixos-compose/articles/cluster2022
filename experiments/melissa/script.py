from execo import Process, SshProcess, Remote, Put
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_g5k.kadeploy import deploy, Deployment

import os
import sys
import time
import yaml
import shutil
import pathlib


class Expe:
    def __init__(self, env_file, melissa_path):
        self.nb_nodes = 1
        self.site = "grenoble"
        self.cluster = "dahu"
        self.realpath = pathlib.Path(__file__).parent.absolute()
        self.current_path = f"{os.environ['HOME']}/.{str(self.realpath)[1:]}"
        self.env_file = f"{os.environ['HOME']}/.{env_file[1:]}"
        self.build_path = "/root/cluster2022"
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, walltime=2*60*60)
        self.oar_job_id, _ = oar_job[0]
        self.build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]
        self.melissa_path = melissa_path

        self.deploy_nix_image()

        self.setup()

    def deploy_nix_image(self):
        deploy(Deployment(hosts=[self.build_node], user='root', env_file=self.env_file))

    def setup(self):
        self.path_nix = "export PATH=\"$PATH:/nix/var/nix/profiles/default/bin\";"
        # install git
        Remote(f"{self.path_nix} nix-env -iA nixpkgs.git", self.build_node, connection_params={'user':'root'}).run()
        # install jq
        Remote(f"{self.path_nix} nix-env -iA nixpkgs.jq", self.build_node, connection_params={'user':'root'}).run()
        # flakos
        # Remote(f"mkdir -p /root/.config/nix", self.build_node, connection_params={'user':'root'}).run()
        Remote(f"{self.path_nix} echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf", self.build_node, connection_params={'user':'root'}).run()
        # scp
        Remote(f"mkdir -p {self.build_path}", self.build_node, connection_params={'user':'root'}).run()
        local_filenames = ["flake.nix", "flake.lock", "composition.nix", "melissa-heat-pde.nix", "melissa.nix", "mpitest.c", "nfs.nix", "slurmconfig.nix", "slurmdbd.nix"]
        local_files = [os.path.join(self.melissa_path, filename) for filename in local_filenames]
        Put(self.build_node, local_files=local_files, remote_location=self.build_path, connection_params={'user':'root'}).run()
        # git init
        Remote(f"{self.path_nix} cd {self.build_path} && git init && git add *", self.build_node, connection_params={'user':'root'}).run()

    def clean_store(self):
        build_cmd = f"{self.path_nix} cd {self.build_path} && rm result"
        build_remote = Remote(build_cmd, self.build_node, connection_params={'user':'root'})
        build_remote.run()

        build_cmd = f"{self.path_nix} nix-collect-garbage -d"
        build_remote = Remote(build_cmd, self.build_node, connection_params={'user':'root'})
        build_remote.run()

    def build_with_flavour(self, flavour):
        build_cmd = f"{self.path_nix} cd {self.build_path} && nix build .#composition::{flavour} --max-jobs 64"
        build_remote = Remote(build_cmd, self.build_node, connection_params={'user':'root'})
        build_remote.run()

    def build_pipeline(self, flavours):
        times = {}
        self.clean_store()
        for flavour in flavours:
            time_before = time.time()
            self.build_with_flavour(flavour)
            time_after = time.time()
            times[flavour] = time_after - time_before
        return times

    def build_one_by_one(self, flavours):
        times = {}
        for flavour in flavours:
            self.clean_store()
            time_before = time.time()
            self.build_with_flavour(flavour)
            time_after = time.time()
            times[flavour] = time_after - time_before
        return times

    def destroy(self):
        oardel([(self.oar_job_id, self.site)])
        pass

def reserve_nodes(nb_nodes, site, cluster, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh", "deploy"]), site)])
    return jobs

def main():
    args = sys.argv
    expe = Expe(args[1], args[2])
    flavours = ["docker", "vm-ramdisk", "g5k-ramdisk", "g5k-image"]
    print("docker, vm-ramdisk, g5k-ramdisk, g5k-image")
    for _ in range(5):
        times = expe.build_pipeline(flavours)
        print(f"{times['docker']}, {times['vm-ramdisk']}, {times['g5k-ramdisk']}, {times['g5k-image']}, pipeline")

    for _ in range(5):
        times = expe.build_one_by_one(flavours)
        print(f"{times['docker']}, {times['vm-ramdisk']}, {times['g5k-ramdisk']}, {times['g5k-image']}, one-by-one")
    expe.destroy()


if __name__ == "__main__":
    main()
