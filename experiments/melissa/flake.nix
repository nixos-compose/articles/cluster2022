{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose";
  };

  outputs = { self, nixpkgs, nxc }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    nixos-compose = nxc.defaultPackage.${system};
    nxcEnv = nixos-compose.dependencyEnv;
  in
  {
    packages.${system} = {
		run_expe = pkgs.writeScriptBin "run_expe" ''
			${nxcEnv}/bin/python3 ${
			  ./script.py
            } ${./nix_deb.yaml} ${./nxc}
		  '';
    };
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [
          pkgs.python3
          nxc.packages.${system}.nixos-compose
        ];
      };
    };
  };
}
