{ lib, pkgs, flavour,... }: {
  nodes = let
    nComputeNode = "10";
    dbdConfig = import ./slurmdbd.nix { inherit pkgs; };
    nfsConfigs = import ./nfs.nix { inherit flavour; };
    slurmconfig = import ./slurmconfig.nix { inherit pkgs nComputeNode; };
    melissa = import ./melissa.nix { inherit pkgs; };
    melissa-heat-pde = import ./melissa-heat-pde.nix { inherit pkgs melissa; };

  in {
    dbd = { pkgs, ... }: {
      imports = [ dbdConfig ];
      systemd.services.slurmdbd.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    server = { pkgs, ... }: {
      imports = [ slurmconfig nfsConfigs.server ];
      services.slurm.server.enable = true;
      systemd.services.slurmctld.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    computeNode = { pkgs, ... }: {
      imports = [ slurmconfig nfsConfigs.client ];
      environment.systemPackages = [ melissa melissa-heat-pde ];
      services.slurm.client.enable = true;
      systemd.services.slurmd.serviceConfig = {
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    frontend = { pkgs, ... }:
      let
        heat-script = pkgs.writeScriptBin "heatScript" ''
          OPTIONS=''${1-${melissa-heat-pde}/options.py}
          cd /data
          rm -r melissa-results
          ${melissa}/bin/melissa-launcher --output-dir='melissa-results' --scheduler-arg='--account=abc@cpu' slurm $OPTIONS heatc
        '';
        my-python-packages = python-packages:
          with python-packages;
          [ matplotlib ];
        python-with-my-packages = pkgs.python3.withPackages my-python-packages;
        plot-script = pkgs.writeScriptBin "plotScript" ''
          PLOT=${melissa-heat-pde}/plot-results.py
          cd /data
          ${python-with-my-packages}/bin/python3 $PLOT melissa-results temperature mean
        '';
      in {
        imports = [ slurmconfig nfsConfigs.client ];
        services.slurm = { enableStools = true; };
        environment.systemPackages = [ pkgs.ffmpeg heat-script plot-script melissa melissa-heat-pde];
      };
  };

  testScript = ''
        start_all()

        # Make sure DBD is up after DB initialzation
        with subtest("can_start_slurmdbd"):
            dbd.succeed("systemctl restart slurmdbd")
            dbd.wait_for_unit("slurmdbd.service")
            dbd.wait_for_open_port(6819)

        # there needs to be an entry for the current
        # cluster in the database before slurmctld is restarted
        with subtest("add_account"):
            server.succeed("sacctmgr -i add cluster default")
            # check for cluster entry
            server.succeed("sacctmgr list cluster | awk '{ print $1 }' | grep default")

        with subtest("can_start_slurmctld"):
            server.succeed("systemctl restart slurmctld")
            server.wait_for_unit("slurmctld.service")

    ## REWRITE THIS TEST NODES HAVE CHANGED
        # with subtest("can_start_slurmd"):
        #     for node in [node1, node2, node3]:
        #         node.succeed("systemctl restart slurmd.service")
        #         node.wait_for_unit("slurmd")

        # Test that the cluster works and can distribute jobs;

        # with subtest("run_distributed_command"):
        #     # Run `hostname` on 3 nodes of the partition (so on all the 3 nodes).
        #     # The output must contain the 3 different names
        #     frontend.succeed("srun -N 3 hostname | sort | uniq | wc -l | xargs test 3 -eq")

            with subtest("check_slurm_dbd"):
                # find the srun job from above in the database
                server.succeed("sleep 5")
                server.succeed("sacct | grep hostname")

        with subtest("run_PMIx_mpitest"):
            frontend.succeed("srun -N 3 --mpi=pmix mpitest | grep size=3")

        with subtest("check if melissa binaries working"):
          frontend.succeed("melissa-config --version")
          frontend.succeed("melissa-server -h")
          frontend.succeed("melissa-launcher -h")

      '';
}
