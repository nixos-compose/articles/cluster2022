{ pkgs, nComputeNode }:
let
  mpitest =
    let mpitestC = pkgs.writeText "mpitest.c" (builtins.readFile ./mpitest.c);
    in pkgs.runCommand "mpitest" { } ''
      mkdir -p $out/bin
      ${pkgs.openmpi}/bin/mpicc ${mpitestC} -o $out/bin/mpitest
    '';
in {
  services.openssh.forwardX11 = false;
  services.slurm = {
    controlMachine = "server";
    nodeName = [ "computeNode[1-${nComputeNode}] CPUs=1 State=UNKNOWN" ];
    partitionName = [
      "debug Nodes=computeNode[1-${nComputeNode}] Default=YES MaxTime=INFINITE State=UP"
    ];
    extraConfig = ''
      AccountingStorageHost=dbd
      AccountingStorageType=accounting_storage/slurmdbd
      MpiDefault=pmix
    '';
  };

  environment.systemPackages = [ mpitest ];

  networking.firewall.enable = false;
  systemd.tmpfiles.rules = [
    "f /etc/munge/munge.key 0400 munge munge - mungeverryweakkeybuteasytointegratoinatest"
  ];
}
