import argparse
import os
import enoslib as en


SITE = "grenoble"
CLUSTER = "dahu"


class Expe:
    def __init__(self):
        self.solution = "enoslib"
        self.expe = "flent"
        self.network = en.G5kNetworkConf(type="prod", roles=["public"], site=SITE)
        self.conf = (
                en.G5kConf.from_settings(job_name="enoslib_several_networks", job_type="deploy", env_name="https://api.grid5000.fr/sid/sites/grenoble/public/qguilloteau/cluster2022/my_image.desc")
            .add_network_conf(self.network)
            .add_machine(
                roles=["server", "xp"],
                cluster=CLUSTER,
                nodes=1,
                primary_network=self.network,
            )
            .add_machine(
                roles=["client", "xp"],
                cluster=CLUSTER,
                nodes=1,
                primary_network=self.network,
            )
            .finalize()
        )
        self.provider = en.G5k(self.conf)

    def init(self):
        self.roles, self.networks = self.provider.init()

    def provisionning(self):
        with en.actions(roles=self.roles) as p:
            p.apt(
                name=["flent", "netperf", "python3-setuptools", "python3-matplotlib"],
                state="present",
            )
        with en.actions(pattern_hosts="server", roles=self.roles) as p:
            p.shell("nohup netperf &")

    def run(self):
        with en.actions(pattern_hosts="client", roles=self.roles) as p:
            # get the address of server
            server_address = self.roles["server"][0].address
            p.shell(
                "flent rrul -p all_scaled "
                + "-l 60 "
                + f"-H { server_address } "
                + "-t 'bufferbloat test' "
                + "-o /tmp/result.png"
            )
            p.fetch(src="/tmp/result.png", dest=f"{os.environ['HOME']}/result")

    def destroy(self):
        self.provider.destroy()

    def build(self):
        pass

    def deploy(self):
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--bench")
    args = parser.parse_args()

    expe = Expe()
    expe.init()
    expe.provisionning()

    expe.run()

    expe.destroy()
