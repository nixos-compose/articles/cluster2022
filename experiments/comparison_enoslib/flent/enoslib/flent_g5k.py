import enoslib as en
import logging

SITE = "grenoble"
CLUSTER = "dahu"

_ = en.init_logging()

network = en.G5kNetworkConf(type="prod", roles=["public"], site=SITE)

conf = (
    en.G5kConf.from_settings(job_name="enoslib_several_networks")
        .add_network_conf(network)
        .add_machine(
            roles=["server", "xp"],
            cluster=CLUSTER,
            nodes=1,
            primary_network=network,
        )
        .add_machine(
            roles=["client", "xp"],
            cluster=CLUSTER,
            nodes=1,
            primary_network=network,
        )
        .finalize()
)

provider = en.G5k(conf)

# The code below is intended to be provider agnostic

# Start the resources
roles, networks = provider.init()

# Experimentation logic starts here
with en.actions(roles=roles) as p:
    p.apt(
        name=["flent", "netperf", "python3-setuptools", "python3-matplotlib"],
        state="present",
    )

with en.actions(pattern_hosts="server", roles=roles) as p:
    p.shell("nohup netperf &")

with en.actions(pattern_hosts="client", roles=roles) as p:
    # get the address of server
    server_address = roles["server"][0].address
    p.shell(
        "flent rrul -p all_scaled "
        + "-l 60 "
        + f"-H { server_address } "
        + "-t 'bufferbloat test' "
        + "-o result.png"
    )
    p.fetch(src="result.png", dest="result")

provider.destroy()