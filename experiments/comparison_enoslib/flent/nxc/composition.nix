{ pkgs, ... }:
let
  sysPackages = with pkgs; [
    python3
    python3Packages.matplotlib
    python3Packages.setuptools
    netperf
    flent
  ];
in {
  nodes = {
    server = { pkgs, ... }: {
      networking.firewall.allowedTCPPorts = [ 12865 ];
      networking.firewall.allowedUDPPorts = [ 12865 ];

      environment.systemPackages = sysPackages;

      systemd.services.netperf = {
        script = ''
          ${pkgs.netperf}/bin/netserver -p 12865
          sleep infinity
        '';
        wantedBy = [ "multi-user.target" ];
        serviceConfig = { Type = "oneshot"; };
      };
    };
    client = { pkgs, ... }:
      let
        flent-script = pkgs.writeScriptBin "flentScript" ''
          flent rrul -p all_scaled -l 60 -H server -t 'bufferbloat test' -o result.png
        '';
      in { environment.systemPackages = sysPackages ++ [ flent-script ]; };
  };

  testScript = ''
    start_all()

    server.wait_for_open_port(12865)
    server.wait_for_unit("netperf.service")
    client.succeed("flent -h")
  '';
}
