from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo, get_build_node, build_derivation

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os
import pathlib

OAR_JOB_ID=None

class Expe:
    def __init__(self):
        self.solution = "nxc"
        self.expe = "flent"
        self.nb_nodes = 2
        self.roles_quantities = {"server": ["server"], "client": ["client"]}
        self.site = "grenoble"
        self.cluster = "dahu"
        self.nxc_folder = pathlib.Path(__file__).parent.absolute()#os.getcwd() #FIXME
        self.composition_name = "composition"
        self.clean = True
        self.flavour = "g5k-ramdisk"
        self.nix_chroot_script = os.path.join(self.nxc_folder, "nix-user-chroot.sh")
        self.job_time_type = "night"

    def init(self):
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, "deploy" if self.flavour == "g5k-image" else "allow_classic_ssh", walltime=15*60, job_time_type=self.job_time_type)
        self.oar_job_id, site = oar_job[0]
        # (self.job_id, self.build_node) = get_build_node(self.site, self.cluster, [], walltime=60*60)

    def build(self):
        (self.nxc_build_file, time, size) = build_nxc_execo(self.nxc_folder,
                                                            self.site, self.cluster,
                                                            flavour=self.flavour,
                                                            walltime=15*60,
                                                            extra_job_type=[self.job_time_type],
                                                            composition_name=self.composition_name,
                                                            nix_chroot_script=self.nix_chroot_script,
                                                            clean_store=self.clean)
        return time

    def deploy(self):
        self.nxc_build_file = f"{os.environ['HOME']}/execo_build"
        self.nodes = get_oar_job_nodes_nxc(self.oar_job_id,
                                           self.site,
                                           flavour_name=self.flavour,
                                           compose_info_file=self.nxc_build_file,
                                           composition_name=self.composition_name,
                                           roles_quantities=self.roles_quantities)
        print(self.nodes)


    def provisionning(self):
        pass

    def run(self):
        server_addr = self.nodes["server"][0].address
        Remote(f"flent rrul -p all_scaled -l 60 -H {server_addr} -t 'bufferbloat test' -o /tmp/result.png", self.nodes["client"]).run()


    def destroy(self):
        oardel([(self.oar_job_id, self.site)])

def reserve_nodes(nb_nodes, site, cluster, job_type, walltime=3600, job_time_type="day"):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type, job_time_type]), site)])
    return jobs

if __name__ == "__main__":
    expe = Expe()
    expe.init()
    expe.build()
    expe.deploy()
    expe.provisionning()
    expe.run()
    expe.destroy()
