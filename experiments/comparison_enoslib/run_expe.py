import time
import os
import sys
import importlib.machinery
import importlib.util
from pathlib import Path

here_path = Path(__file__).parent.absolute()

def get_driver(path):
  """
  Get the path to the driver file
  """
  return os.path.join(path, "script.py")

def get_driver_module(driver_path):
  """
  Loads the driver
  """
  # Get path to mymodule
  module = driver_path.split('/')[-1]
  loader = importlib.machinery.SourceFileLoader(module, driver_path)
  spec = importlib.util.spec_from_loader(module, loader)
  return loader, importlib.util.module_from_spec(spec)

def load_module(loader, module):
  """
  Loads the module of the driver
  """
  loader.exec_module(module)

def evaluate_expe(expe):
  # init
  time_before_init = time.time()
  expe.init()
  time_after_init = time.time()

  # build
  time_before_build = time.time()
  actual_build_time = expe.build()
  time_after_build = time.time()

  # deploy
  time_before_deploy = time.time()
  expe.deploy()
  time_after_deploy = time.time()
  
  # provisionning
  time_before_prov = time.time()
  expe.provisionning()
  time_after_prov = time.time()

  # run
  time_before_run = time.time()
  expe.run()
  time_after_run = time.time()
  
  expe.destroy()

  stats = {"init": time_after_init - time_before_init,
       "build": actual_build_time if actual_build_time else (time_after_build - time_before_build),
       "deploy": time_after_deploy - time_before_deploy,
       "prov": time_after_prov - time_before_prov,
             "run": time_after_run - time_before_run}

  return stats

def run(path_folder, flavour="g5k-image"):
  print(here_path)
  print(path_folder)
  folder_path = os.path.join(here_path, path_folder)
  print(folder_path)
  driver_path = get_driver(folder_path)
  
  loader, driver_module = get_driver_module(driver_path)
  load_module(loader, driver_module)
  
  expe = driver_module.Expe()
  expe.flavour = flavour
  
  stats = evaluate_expe(expe)
  print(f"{expe.solution}, {expe.expe}, {expe.flavour}, {stats['init']}, {stats['build']}, {stats['deploy']}, {stats['prov']}, {stats['run']}")

def main():
  args = sys.argv
  path_folder = args[1]
  print(path_folder)
  flavour = "g5k-image"
  if len(args) > 2:
    flavour = args[2]
  run(path_folder, flavour)

if __name__ == "__main__":
  main()

  

# folder_path = os.path.join(here_path, "k3s/nxc/")
# if os.path.exists(os.path.join(folder_path, "nxc.json")):
#     os.symlink(os.path.join(folder_path, "nxc.json"), os.path.join(here_path, "nxc.json"))
# # driver_path = get_driver(os.path.join(here_path, "k3s/enoslib/"))
# driver_path = get_driver(folder_path)
# 
# loader, driver_module = get_driver_module(driver_path)
# load_module(loader, driver_module)
# 
# expe = driver_module.Expe()
# 
# stats = evaluate_expe(expe)
# print(f"stats: {stats}")
# 
