{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?rev=3e644bd62489b516292c816f70bf0052c693b3c7";
    qornflakes.url = "github:guilloteauq/qornflakes";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose";
  };

  outputs = { self, nixpkgs, qornflakes, nxc }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    qorn = qornflakes.packages.${system};

    nixos-compose = nxc.defaultPackage.${system};
    nxcEnv = nixos-compose.dependencyEnv;

    plop = pkgs.writeScriptBin "plop" ''
        ${nxcEnv}/bin/python3 ${./run_expe.py} ${./k3s/nxc}
    '';

    myPython = (pkgs.python3.withPackages (ps: [qorn.enoslib]));
    enos_k3s = pkgs.writeScriptBin "enos_k3s" ''
        ${myPython}/bin/python3 ${./run_expe.py} ${./k3s/enoslib}
    '';

  in
  {
    packages.${system} = {
        enos_k3s = pkgs.writeScriptBin "enos_k3s" ''
            ${myPython}/bin/python3 ${./run_expe.py} ${./k3s/enoslib}
        '';
        enos_flent = pkgs.writeScriptBin "enos_flent" ''
            ${myPython}/bin/python3 ${./run_expe.py} ${./flent/enoslib}
        '';
        nxc_k3s_ramdisk = pkgs.writeScriptBin "nxc_k3s_ramdisk" ''
            ${nxcEnv}/bin/python3 ${./run_expe.py} ${./k3s/nxc} g5k-ramdisk
        '';
        nxc_k3s_image = pkgs.writeScriptBin "nxc_k3s_image" ''
            ${nxcEnv}/bin/python3 ${./run_expe.py} ${./k3s/nxc} g5k-image
        '';
        nxc_flent_ramdisk = pkgs.writeScriptBin "nxc_flent_ramdisk" ''
            ${nxcEnv}/bin/python3 ${./run_expe.py} ${./flent/nxc} g5k-ramdisk
        '';
        nxc_flent_image = pkgs.writeScriptBin "nxc_flent_image" ''
            ${nxcEnv}/bin/python3 ${./run_expe.py} ${./flent/nxc} g5k-image
        '';
    };
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [
          pkgs.python3
          qorn.enoslib
          nxc.packages.${system}.nixos-compose
        ];
      };
    };
  };
}
