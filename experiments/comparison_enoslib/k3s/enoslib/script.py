import enoslib as en
from enoslib.api import actions, run
from typing import List

from enoslib.objects import Host, Roles

import time
import pathlib
import os

here_path = pathlib.Path(__file__).parent.absolute()

_ = en.init_logging()

class K3s:
    def __init__(self, master: List[Host], agent: List[Host]):
        self.master = master
        self.agent = agent
        self.roles = Roles(master=self.master, agent=self.agent)

    def deploy(self):
        with actions(roles=self.roles) as p:
            p.apt(name="curl", state="present")

        with actions(pattern_hosts="master", roles=self.roles, gather_facts=False) as p:
            p.shell("curl -sfL https://get.k3s.io | sh")
        # Getting the token
        result = run(
            "cat /var/lib/rancher/k3s/server/node-token",
            pattern_hosts="master",
            roles=self.roles,
        ).to_dict()
        print(result)
        token = result[0]["stdout"]
        with actions(pattern_hosts="agent", roles=self.roles, gather_facts=False) as p:
            cmd = f"K3S_URL=https://{self.master[0].address}:6443 K3S_TOKEN={token} sh -"
            p.shell(
                (
                    f"curl -sfL https://get.k3s.io | {cmd}"
                )
            )
            print(p.results.to_dict())
    def destroy(self):
        pass

    def backup(self):
        pass


class Expe:
    def __init__(self):
        self.solution = "enoslib"
        self.expe = "k3s"
        self.network = en.G5kNetworkConf(type="prod", roles=["my_network"], site="grenoble")
        self.conf = (
            en.G5kConf.from_settings(job_type="deploy", job_name="plop", env_name="https://api.grid5000.fr/sid/sites/grenoble/public/qguilloteau/cluster2022/my_image.desc")
            .add_network_conf(self.network)
            .add_machine(
                roles=["master"], cluster="dahu", nodes=1, primary_network=self.network
            )
            .add_machine(
                roles=["agent"],
                cluster="dahu",
                nodes=1,
                primary_network=self.network,
            )
            .finalize()
        )

        self.provider = en.G5k(self.conf)

    def init(self):
        self.roles, self.networks = self.provider.init()

    def provisionning(self):
        k3s = K3s(master=self.roles["master"], agent=self.roles["agent"])
        k3s.deploy()


        store_path = f"{os.environ['HOME']}/.{str(here_path)[1:]}"
        config_file = os.path.join(store_path, "nginx.yaml")

        while True:
            result_wait = en.run("k3s kubectl get nodes", roles=self.roles["master"]).to_dict()
            if result_wait[0]["stdout"] != "No resources found":
                break

        # waiting for all the agents to register
        while True:
            result_wait = en.run_command("k3s kubectl get nodes | grep '\\<Ready\\>'", roles=self.roles["master"], on_error_continue=True).to_dict()
            print(result_wait)
            if len(result_wait) > 0 and result_wait[0]["stdout"].count('\n') + 1 == len(self.roles["master"]) + len(self.roles["agent"]):
                break
        result_namespace = en.run("k3s kubectl create namespace test", self.roles["master"])

        result_apply = en.run(f"k3s kubectl apply -n test -f {config_file}", roles=self.roles["master"])

        while True:
            result_wait = en.run("k3s kubectl get pods -n test | grep 'Running'", roles=self.roles["master"], on_error_continue=True).to_dict()
            if len(result_wait[0]["stdout"]) > 0:
                break
    def run(self):
        result_cluster_ip = en.run("k3s kubectl get services -n test | grep 'test-nginx-service' | awk '{print $3;}'", self.roles["master"]).to_dict()
        cluster_ip = result_cluster_ip[0]["stdout"]

        result_curl = en.run(f"curl {cluster_ip}", self.roles["master"])
        print(result_curl.to_dict()[0]["stdout"])

    def destroy(self):
        self.provider.destroy()

    def build(self):
        pass

    def deploy(self):
        pass

if __name__ == "__main__":
    expe = Expe()
    expe.init()
    expe.provisionning()
    expe.run()
    expe.destroy()
