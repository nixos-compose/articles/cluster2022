from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo, get_build_node, build_derivation

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os
import pathlib

OAR_JOB_ID=None

class Expe:
    def __init__(self):
        self.solution = "nxc"
        self.expe = "k3s"
        self.nb_nodes = 2
        self.roles_quantities = {"server": ["server"], "agent": ["agent"]}
        self.site = "grenoble"
        self.cluster = "dahu"
        self.nxc_folder = pathlib.Path(__file__).parent.absolute()#os.getcwd() #FIXME
        self.composition_name = "composition"
        self.clean = True
        self.flavour = "g5k-ramdisk"
        self.nix_chroot_script = os.path.join(self.nxc_folder, "nix-user-chroot.sh")
        self.job_time_type = "night"
		
    def init(self):
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, "deploy" if self.flavour == "g5k-image" else "allow_classic_ssh", walltime=15*60, job_time_type=self.job_time_type)
        self.oar_job_id, site = oar_job[0]
        # (self.job_id, self.build_node) = get_build_node(self.site, self.cluster, [], walltime=30*60)

    def build(self):
        (self.nxc_build_file, time, size) = build_nxc_execo(self.nxc_folder,
                                                            self.site, self.cluster,
                                                            flavour=self.flavour,
                                                            walltime=15*60,
                                                            extra_job_type=[self.job_time_type],
                                                            composition_name=self.composition_name,
                                                            nix_chroot_script=self.nix_chroot_script,
                                                            clean_store=self.clean)
        return time

    def deploy(self):
        print(self.nxc_build_file)
        self.nxc_build_file = f"{os.environ['HOME']}/execo_build"
        print(f"self.flavour = {self.flavour}")
        self.nodes = get_oar_job_nodes_nxc(self.oar_job_id,
                                           self.site,
                                           flavour_name=self.flavour,
                                           compose_info_file=self.nxc_build_file,
                                           composition_name=self.composition_name,
                                           roles_quantities=self.roles_quantities)
        print(self.nodes)


    def provisionning(self):
        # my_command = "k3s kubectl get nodes | grep '\\<Ready\\>'"
        # while True:
        #     get_nodes_remote = Remote(my_command, self.nodes["server"], connection_params={'user': 'root'})
        #     get_nodes_remote.run()
        #     if get_nodes_remote.processes[0].stdout.count('\n') == len(self.nodes):
        #         break
        # create_namespace_remote = Remote("k3s kubectl create namespace test", self.nodes["server"], connection_params={'user': 'root'})
        # create_namespace_remote.run()

        # store_path = f"{os.environ['HOME']}/.{str(self.nxc_folder)[1:]}"
        # apply_remote = Remote(f"k3s kubectl apply -n test -f {os.path.join(store_path, 'nginx.yaml')}" , self.nodes["server"], connection_params={'user': 'root'})
        # apply_remote.run()

        my_command = "k3s kubectl get pods -n test"
        #my_command = "k3s kubectl get pods -n test | grep 'Running'"
        while True:
            get_pods_remote = Remote(my_command, self.nodes["server"], connection_params={'user': 'root'})
            get_pods_remote.run()
            print(get_pods_remote.processes[0].stdout)
            if "Running" in get_pods_remote.processes[0].stdout:
                break

    def run(self):
        get_cluster_ip_remote = Remote("k3s kubectl get services -n test | grep 'test-nginx-service' | awk '{print $3;}'" , self.nodes["server"], connection_params={'user': 'root'})
        get_cluster_ip_remote.run()
        cluster_ip = get_cluster_ip_remote.processes[0].stdout.strip()
        
        get_page_remote = Remote(f"curl {cluster_ip}", self.nodes["server"], connection_params={'user': 'root'})
        get_page_remote.run()

        print(get_page_remote.processes[0].stdout)	


    def destroy(self):
        oardel([(self.oar_job_id, self.site)])

def reserve_nodes(nb_nodes, site, cluster, job_type, walltime=3600, job_time_type="day"):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type, job_time_type]), site)])
    return jobs

if __name__ == "__main__":
    expe = Expe()
    expe.init()
    expe.build()
    expe.deploy()
    expe.provisionning()
    expe.run()
    expe.destroy()
