# NXC Example for K3S


## Build

```
nxc build -f [flavour]
```

note that the `docker` flavour probably does not work.

## Start

```
nxc start
```

## Connect

```
nxc connect
```

## Config k3s

### Check that the agent registered

```
[root@server:~]# k3s kubectl get nodes
NAME     STATUS   ROLES                  AGE   VERSION
server   Ready    control-plane,master   13s   v1.23.4+k3s1
agent    Ready    <none>                 3s    v1.23.4+k3s1
```

### Create a namespace

```
[root@server:~]# k3s kubectl create namespace test
namespace/test created
```

### Start the deployment

The path of the config file is in the `$DEPLOY_FILE` variable

```
[root@server:~]# k3s kubectl apply -n test -f $DEPLOY_FILE
deployment.apps/test-nginx-app unchanged
service/test-nginx-service unchanged
ingress.networking.k8s.io/test-nginx-ingress unchanged
```

### Get status of the pods

```
[root@server:~]# k3s kubectl get pods -n test
NAME                             READY   STATUS    RESTARTS   AGE
test-nginx-app-7bd5bf6f7-sl76r   1/1     Running   0          2m45s
```

```
[root@server:~]# k3s kubectl get services -n test
NAME                 TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
test-nginx-service   ClusterIP   10.43.19.67   <none>        80/TCP    2m51s
```

## Test

Take the IP address given above.

```
[root@server:~]# curl http://10.43.19.67
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

