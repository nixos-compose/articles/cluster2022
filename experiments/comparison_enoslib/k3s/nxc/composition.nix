{ pkgs, flavour, ... }:
let
  # A random token for mutual authentication
  k3sToken = "df54383b5659b9280aa1e73e60ef78fc";

  virtualisationOptions = { ... }:
    if flavour.name == "nixos-test" || flavour.name == "nixos-test-ssh" then {
      virtualisation.memorySize = 1536;
      virtualisation.diskSize = 4096;
    } else
      { };
  serviceRegisterK3s = { lib, pkgs, config, ... }:
    with lib;
    let cfg = config.services.register-k3s;
    in {
      options.services.register-k3s = {
        enable = mkEnableOption "register-k3s service";
        k3sConfig = mkOption {
          type = types.str;
          default = "${./nginx.yaml}";
        };
        k3sNamespace = mkOption {
          type = types.str;
          default = "test";
        };
      };

      config = mkIf cfg.enable {
        systemd.services.register-k3s = {
          after = [ "k3s.service" ];
          wantedBy = [ "multi-user.target" ];
          serviceConfig = {
            # Type = "oneshot";
            Restart = "on-failure";
            RestartSec = 1;
          };
          script = ''
            ${pkgs.k3s}/bin/k3s kubectl create namespace ${cfg.k3sNamespace}

            nodesReady=$(${pkgs.k3s}/bin/k3s kubectl get nodes | grep '/<Ready/>' | wc -l);
            while [ "$masterIsReady" -ge 2 ];
            do
              masterIsReady=$(${pkgs.k3s}/bin/k3s kubectl get nodes | grep '/<Ready/>' | wc -l);
            done

            ${pkgs.k3s}/bin/k3s kubectl apply -n ${cfg.k3sNamespace} -f ${cfg.k3sConfig}
          '';
        };
      };
    };

  commonConfig = { pkgs, ... }: {
    imports = [ virtualisationOptions ];
    environment.systemPackages = with pkgs; [ k3s gzip ];
    environment.sessionVariables = { DEPLOY_FILE = "${./nginx.yaml}"; };
  };

in {
  nodes = {
    server = { pkgs, ... }: {
      imports = [ commonConfig serviceRegisterK3s ];
      networking.firewall.allowedTCPPorts = [ 6443 ];
      services.k3s = {
        enable = true;
        role = "server";
        extraFlags = "--token ${k3sToken}";
      };
      services.register-k3s.enable = true;
    };

    agent = { pkgs, ... }: {
      imports = [ commonConfig ];
      services.k3s = {
        enable = true;
        role = "agent";
        serverAddr = "https://server:6443";
        token = k3sToken;
        # extraFlags = "--node-label app=node-js";
      };
    };
  };

  testScript = ''
    start_all()

    server.wait_for_unit("k3s")
    agent.wait_for_unit("k3s")

    server.succeed("k3s kubectl cluster-info")
  '';
}
