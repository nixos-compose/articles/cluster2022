from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes

import os
import time
import yaml
import shutil
import pathlib


class Expe:
    def __init__(self):
        self.solution = "kameleon"
        self.type = "with_nfs"
        self.nb_nodes = 1
        self.site = "grenoble"
        self.cluster = "dahu"
        self.flavour = "g5k-image"
        self.realpath = pathlib.Path(__file__).parent.absolute()
        self.current_path = f"{os.environ['HOME']}/.{str(self.realpath)[1:]}"
        self.build_path = f"{os.environ['HOME']}/public"
        self.empty_recipe_file = os.path.join(self.current_path, "empty_image.yaml")
        self.hello_recipe_file = os.path.join(self.current_path, "empty_image.yaml")
        with open(self.empty_recipe_file, "r") as recipe_file_content:
            self.empty_recipe = yaml.safe_load(recipe_file_content)
        with open(self.hello_recipe_file, "r") as recipe_file_content:
            self.hello_recipe = yaml.safe_load(recipe_file_content)
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, walltime=2*60*60)
        self.oar_job_id, _ = oar_job[0]
        self.build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]
        self.current_image = "empty_image"


    def clean(self):
        if os.path.exists(os.path.join(self.build_path, "empty_image")):
            shutil.rmtree(os.path.join(self.build_path, "empty_image"))
        if os.path.exists(os.path.join(self.build_path, "hello_image")):
            shutil.rmtree(os.path.join(self.build_path, "hello_image"))

    def _build_image(self, image_name):
        build_cmd = f"kameleon build {os.path.join(self.current_path, image_name)}.yaml -b {self.build_path} -s"
        build_remote = Remote(build_cmd, self.build_node)
        build_remote.run()

    def build(self):
        self._build_image("empty_image")

    def rebuild(self):
        self._build_image("hello_image")

    def get_artifact_size(self, image_name):
        artifact_size = get_folder_size(os.path.join(self.build_path, f"{image_name}"))
        return artifact_size

    def get_image_size(self):
        image_location_folder = os.path.join(self.build_path, self.current_image)
        image_location = os.path.join(image_location_folder, f"{self.current_image}.tar.zst")
        remote_image_size = Remote(f"du {image_location} | cut -f 1", self.build_node)
        remote_image_size.run()
        return int(remote_image_size.processes[0].stdout)

    def change(self):
        self.current_image = "hello_image"
        # self.recipe["setup"].append({'adding_hello': [{'microstep1': [{'exec_in': "apt-get update && apt-get install -y hello"}]}]})
        # with open(os.path.join(self.current_path, "hello_image.yaml"), "w") as hello_image:
        #     yaml.dump(self.recipe, hello_image)

    def destroy(self):
        oardel([(self.oar_job_id, self.site)])


def get_folder_size(folder):
    size = 0
    for path, _, files in os.walk(folder):
        for f in files:
            fp = os.path.join(path, f)
            size += os.stat(fp).st_size
    return size

def reserve_nodes(nb_nodes, site, cluster, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh"]), site)])
    return jobs

if __name__ == "__main__":
    expe = Expe()
    expe.clean()
    expe.build()
    expe.change()
    expe.rebuild()
    expe.destroy()
