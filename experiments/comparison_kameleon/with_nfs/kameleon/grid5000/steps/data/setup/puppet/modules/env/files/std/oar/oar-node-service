#!/bin/sh

# This script is called when oar-node service is started.
# You can change this to use another method to switch the node into the Alive state

# OARSERVER: machine where we remotely run oarnodesetting
OARREMOTE="oar"

# The paths to oarnodecheckquery and oarnodecheckrun (check your installation)
OARNODECHECKQUERY=/usr/bin/oarnodecheckquery
OARNODECHECKRUN=/usr/lib/oar/oarnodecheckrun
# Home directory of user oar
OARHOME=/var/lib/oar

# retry settings
MODSLEEP=20
MINSLEEP=10
MAXRETRY=300

start_oar_node() {
    test -n "$OARREMOTE" || exit 0
    local retry=0
    local sleep=0
    local status=1
    until [ $status -eq 0 ]; do
      echo "oar-node: perform sanity checks"
      $OARNODECHECKRUN
      $OARNODECHECKQUERY
      status=$?
      [ $status -eq 0 ] && {
        echo "oar-node: set the ressources of this node to Alive"
        ssh -t -oStrictHostKeyChecking=no -oPasswordAuthentication=no -i $OARHOME/.ssh/oarnodesetting_ssh.key oar@$OARREMOTE
        status=$?
      }
      [ $status -ne 0 ] && {
        if [ $((retry+=sleep)) -gt $MAXRETRY ]; then
          echo "oar-node: FAILED"
          return 1
        fi
	local random=$RANDOM
        # Workaround for the case where dash is the default shell: dash does
        # not provide $RANDOM
        if [ "x$random" = "x" ]; then
            random=$(bash -c 'echo $RANDOM')
        fi
        sleep=$(($random % $MODSLEEP + $MINSLEEP))
        echo "oar-node: retrying in $sleep seconds..."
        sleep $sleep
      }
    done

    return 0
}

# This function is called when oar-node service is stopped.
# You can change this to use another method to switch the node into the Absent state

stop_oar_node() {
    :
}

usage() {
        echo "Usage: $0 start|stop"
}

if [ $# -eq 0 ]; then
    echo "Missing operation!"
    usage
    exit 1
fi

if [ $1 = "start" ]; then
    start_oar_node
elif [ $1 = "stop" ]; then
    stop_oar_node
else
    echo "Unknown operation!"
    usage
    exit 1
fi
