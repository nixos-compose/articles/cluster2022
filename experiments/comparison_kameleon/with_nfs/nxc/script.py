from execo import Process, SshProcess, Remote, Put
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_g5k.kadeploy import deploy, Deployment
from nixos_compose.nxc_execo import build_derivation

import os
import time
import yaml
import shutil
import pathlib


class Expe:
    def __init__(self):
        self.solution = "nxc"
        self.type = "with_nfs"
        self.nb_nodes = 1
        self.site = "grenoble"
        self.cluster = "dahu"
        self.flavour = "g5k-image"
        self.current_path = pathlib.Path(__file__).parent.absolute()
        # self.current_path = f"{os.environ['HOME']}/.{str(self.realpath)[1:]}"
        self.chroot_script = os.path.join(self.current_path, "nix-user-chroot.sh")
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, walltime=2*60*60)
        self.oar_job_id, _ = oar_job[0]
        self.build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]
        self.do_clean = True
        self.nxc_path = self.current_path

    def clean(self):
        pass

    def _build_image(self, image_name):
        (_, time, size) = build_derivation(self.build_node, self.nxc_path, self.flavour, image_name, self.chroot_script, self.do_clean)
        # (_, time, size) = build_derivation(self.build_node, self.nxc_path, self.flavour, image_name, self.chroot_script, False)
        return time

    def build(self):
        return self._build_image("empty")

    def rebuild(self):
        return self._build_image("hello")

    def get_image_size(self):
        if self.flavour == "g5k-image":
            image_position = "image"
        else:
            image_position = "initrd"
        remote_image_size = Remote("readlink /tmp/execo_build", self.build_node)
        remote_image_size.run()
        store_path = remote_image_size.processes[0].stdout.strip()
        actual_store_path = f"{os.environ['HOME']}/.{store_path[1:]}"

        remote_image_size = Remote(f"jq .all.{image_position} {actual_store_path} -r", self.build_node)
        remote_image_size.run()
        tarball_path = remote_image_size.processes[0].stdout.strip()
        actual_tarball_path = f"{os.environ['HOME']}/.{tarball_path[1:]}"

        md5_remote = Remote(f"md5sum {actual_tarball_path}", self.build_node)
        md5_remote.run()
        print(md5_remote.processes[0].stdout)

        remote_image_size = Remote(f"du {actual_tarball_path} | cut -f 1", self.build_node)
        remote_image_size.run()
        return int(remote_image_size.processes[0].stdout)

    def change(self):
        self.do_clean = not self.do_clean

    def destroy(self):
        oardel([(self.oar_job_id, self.site)])

def reserve_nodes(nb_nodes, site, cluster, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh"]), site)])
    return jobs

if __name__ == "__main__":
    expe = Expe()
    expe.clean()
    expe.build()
    print(expe.get_image_size("empty"))
    expe.change()
    expe.rebuild()
    print(expe.get_image_size("hello"))
    expe.destroy()
