import time
import os
import sys
import importlib.machinery
import importlib.util
from pathlib import Path

here_path = Path(__file__).parent.absolute()

def get_driver(path):
  """
  Get the path to the driver file
  """
  return os.path.join(path, "script.py")

def get_driver_module(driver_path):
  """
  Loads the driver
  """
  # Get path to mymodule
  module = driver_path.split('/')[-1]
  loader = importlib.machinery.SourceFileLoader(module, driver_path)
  spec = importlib.util.spec_from_loader(module, loader)
  return loader, importlib.util.module_from_spec(spec)

def load_module(loader, module):
  """
  Loads the module of the driver
  """
  loader.exec_module(module)

def evaluate_expe(expe):
  # build
  time_before_build = time.time()
  actual_build_time = expe.build()
  time_after_build = time.time()

  build_image_size = expe.get_image_size()

  # change
  expe.change()

  # rebuild
  time_before_rebuild = time.time()
  actual_rebuild_time = expe.rebuild()
  time_after_rebuild = time.time()
  
  rebuild_image_size = expe.get_image_size()

  expe.destroy()

  stats = {
            "build_time": actual_build_time if actual_build_time else (time_after_build - time_before_build),
            "rebuild_time": actual_rebuild_time if actual_rebuild_time else (time_after_rebuild - time_before_rebuild),
            "build_size": build_image_size,
            "rebuild_size": rebuild_image_size,
       }

  return stats
  


def run(path_folder, flavour="g5k-image"):
  folder_path = os.path.join(here_path, path_folder)
  driver_path = get_driver(folder_path)
  
  loader, driver_module = get_driver_module(driver_path)
  load_module(loader, driver_module)
  
  expe = driver_module.Expe()
  expe.flavour = flavour
  
  stats = evaluate_expe(expe)
  print(f"{expe.solution}, {expe.type}, {expe.flavour}, {stats['build_time']}, {stats['rebuild_time']}, {stats['build_size']}, {stats['rebuild_size']}")


def main():
  args = sys.argv
  path_folder = args[1]
  flavour = "g5k-image"
  if len(args) > 2:
    flavour = args[2]
  run(path_folder, flavour)

if __name__ == "__main__":
    main()
