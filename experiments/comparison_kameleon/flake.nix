{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose";
  };

  outputs = { self, nixpkgs, nxc }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    nixos-compose = nxc.defaultPackage.${system};
    nxcEnv = nixos-compose.dependencyEnv;
  in
  {
    packages.${system} = {
		run_expe_without_nfs_nxc_image = pkgs.writeScriptBin "run_expe_without_nfs_nxc_image" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./without_nfs/nxc
            } g5k-image
		  '';
		run_expe_without_nfs_nxc_ramdisk = pkgs.writeScriptBin "run_expe_without_nfs_nxc_ramdisk" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./without_nfs/nxc
            } g5k-ramdisk
		  '';
		run_expe_without_nfs_kam_image = pkgs.writeScriptBin "run_expe_without_nfs_kam_image" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./without_nfs/kameleon
            } g5k-image
		  '';
		run_expe_with_nfs_nxc_image = pkgs.writeScriptBin "run_expe_with_nfs_nxc_image" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./with_nfs/nxc
            } g5k-image
		  '';
		run_expe_with_nfs_nxc_ramdisk = pkgs.writeScriptBin "run_expe_with_nfs_nxc_ramdisk" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./with_nfs/nxc
            } g5k-ramdisk
		  '';
		run_expe_with_nfs_kam_image = pkgs.writeScriptBin "run_expe_with_nfs_kam_image" ''
			${nxcEnv}/bin/python3 ${
			  ./run_expe.py
			} ${
              ./with_nfs/kameleon
            } g5k-image
		  '';
    };
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [
          pkgs.python3
          nxc.packages.${system}.nixos-compose
        ];
      };
    };
  };
}
