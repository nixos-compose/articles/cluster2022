from execo import Process, SshProcess, Remote, Put
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_g5k.kadeploy import deploy, Deployment

import os
import time
import yaml
import shutil
import pathlib


class Expe:
    def __init__(self):
        self.solution = "nxc"
        self.type = "without_nfs"
        self.nb_nodes = 1
        self.site = "grenoble"
        self.cluster = "dahu"
        self.flavour = "g5k-image"
        self.realpath = pathlib.Path(__file__).parent.absolute()
        self.current_path = f"{os.environ['HOME']}/.{str(self.realpath)[1:]}"
        self.env_file = os.path.join(self.current_path, "nix_deb.yaml")
        self.build_path = "/root/cluster2022"
        oar_job = reserve_nodes(self.nb_nodes, self.site, self.cluster, walltime=2*60*60)
        self.oar_job_id, _ = oar_job[0]
        self.build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]

        self.deploy_nix_image()

        self.setup()

    def deploy_nix_image(self):
        deploy(Deployment(hosts=[self.build_node], user='root', env_file=self.env_file))

    def setup(self):
        self.path_nix = "export PATH=\"$PATH:/nix/var/nix/profiles/default/bin\";"
        # install git
        Remote(f"{self.path_nix} nix-env -iA nixpkgs.git", self.build_node, connection_params={'user':'root'}).run()
        # install jq
        Remote(f"{self.path_nix} nix-env -iA nixpkgs.jq", self.build_node, connection_params={'user':'root'}).run()
        # flakos
        # Remote(f"mkdir -p /root/.config/nix", self.build_node, connection_params={'user':'root'}).run()
        Remote(f"{self.path_nix} echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf", self.build_node, connection_params={'user':'root'}).run()
        # scp
        Remote(f"mkdir -p {self.build_path}", self.build_node, connection_params={'user':'root'}).run()
        local_filenames = ["flake.nix", "flake.lock", "compositions.nix", "composition_empty.nix", "composition_hello.nix"]
        local_files = [os.path.join(self.current_path, filename) for filename in local_filenames]
        Put(self.build_node, local_files=local_files, remote_location=self.build_path, connection_params={'user':'root'}).run()
        # git init
        Remote(f"{self.path_nix} cd {self.build_path} && git init && git add *", self.build_node, connection_params={'user':'root'}).run()

    def clean(self):
        pass

    def _build_image(self, image_name):
        build_cmd = f"{self.path_nix} cd {self.build_path} && nix build .#{image_name}::{self.flavour} --max-jobs 64"
        build_remote = Remote(build_cmd, self.build_node, connection_params={'user':'root'})
        build_remote.run()

    def build(self):
        self._build_image("empty")

    def rebuild(self):
        self._build_image("hello")

    def get_artifact_size(self, image_name):
        return -1

    def get_image_size(self):
        if self.flavour == "g5k-image":
            image_position = "image"
        else:
            image_position = "initrd"
        remote_image_size = Remote(f"{self.path_nix} jq .all.{image_position} {self.build_path}/result -r | xargs -I {{}} du {{}} | cut -f 1", self.build_node, connection_params={'user':'root'})
        remote_image_size.run()

        md5_remote = Remote(f"{self.path_nix} jq .all.{image_position} {self.build_path}/result -r", self.build_node, connection_params={'user':'root'})
        md5_remote.run()
        path_tarball = md5_remote.processes[0].stdout.strip()

        md5_remote = Remote(f"md5sum {path_tarball}", self.build_node, connection_params={'user':'root'})
        md5_remote.run()
        print(md5_remote.processes[0].stdout)
        return int(remote_image_size.processes[0].stdout)

    def change(self):
        pass

    def destroy(self):
        #oardel([(self.oar_job_id, self.site)])
        pass

def reserve_nodes(nb_nodes, site, cluster, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh", "deploy"]), site)])
    return jobs

if __name__ == "__main__":
    expe = Expe()
    expe.clean()
    expe.build()
    expe.change()
    expe.rebuild()
    expe.destroy()
