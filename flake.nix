{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nixpkgs2111.url = "github:nixos/nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs, nixpkgs2111 }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    pkgs2111 = import nixpkgs2111 { inherit system; };
    rPkgs = with pkgs2111.rPackages; [
      rlang
      tidyverse
      zoo
      modelr
      patchwork
      treemap
      #gridExtra
      #DoE_wrapper
      reshape2
      #profvis
      #tidyjson
    ];
    myR = pkgs2111.rWrapper.override { packages = rPkgs; };
  in
  {
    packages.${system} = rec {
      cluster22 = pkgs.stdenv.mkDerivation {
        name = "cluster22";
        src = ./paper;
        buildInputs = with pkgs; [
          pandoc
          texlive.combined.scheme-full
          ninja
          bibtool
          rubber
        ];
        buildPhase = ''
          mkdir -p $out
          ninja
          cp main.pdf $out
        '';
        installPhase = ''
          echo "Skipping installPhase"
        '';
      };
      default = cluster22;
    };
    devShells.${system} = {
      rshell = pkgs.mkShell {
        buildInputs = with pkgs; [
          myR
        ];
      };
      style = pkgs.mkShell {
        buildInputs = with pkgs; [
          vale
          languagetool
        ];
      };
      default = pkgs.mkShell {
        buildInputs = with pkgs; [
          pandoc
          texlive.combined.scheme-full
          ninja
          vale
          bibtool
          rubber
          entr
        ];
        shellHook = ''
          ls main.md sections/*.md | entr -s "ninja"
        '';
      };
    };
  };
}
